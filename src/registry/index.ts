import { promises as _fs } from "../utils/fs";
import _path from "path";
import _os from "os";

import { PackageType, getCompilerOptions } from "../build/base";
import _log from '../utils/logger';

import _pkg from "../../package.json";
import { compile } from "../build/compiler";
import { pkg } from "../build/packager";

export async function getPath(pkg: { id: string, version: string, type: PackageType }) : Promise<string> {
	// TODO: make platform-agnostic?
	const pkgDir = _path.join(_os.homedir(), ".local", "share", _pkg.name, "registry");
	await _fs.mkdir(pkgDir, { recursive: true });
	return _path.join(pkgDir, getName(pkg));
}

export function getName(pkg: { id: string, version: string, type: PackageType }) : string {
	return `${pkg.id}-${pkg.version}.${pkg.type}.zip`;
}

export async function resolveLocal(cwd: string, type: PackageType, pkgStr: string) : Promise<string> {
	const [pkgType, pkgName] = pkgStr.split(":");

	switch (pkgType) {
		case "http":
		case "https":
			// TODO: download file to os.tmpdir()
			break;
		case "file":
			// already a compiled zip: return location
			if (pkgName.endsWith(".zip"))
				return _path.join(cwd, pkgName);

			// refers to another package: compile it first!
			const pkgDir = _path.join(cwd, pkgName);
			const opts = await getCompilerOptions(pkgDir, type);
			await pkg(pkgDir, type, await compile(pkgDir, type));
			return _path.join(pkgDir, opts.outDir, opts.zipFile);
		case "local":
			const [pkgID, pkgVersion] = pkgName.split("@");
			// TODO: resolve local packages
			break;
		default:
	}

	throw `Couldn't find a package for dependency: ${pkgStr}`;
}

export async function publishLocal(cwd: string, type: PackageType) {
	const opts = await getCompilerOptions(cwd, type);
	const path = await getPath(opts);

	const buffer = await _fs.readFile(opts.zipFile);
	await _fs.writeFile(path, buffer);
	_log.info(`Published "local:${opts.id}" ${opts.version}`);
}
