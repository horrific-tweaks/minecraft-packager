import _parseArgs from "arg";

import _init from "./commands/init";
import _build from "./commands/build";
import _install from "./commands/install";
import _publish from "./commands/publish";

import _log from "./utils/logger";

import _pkg from "../package.json";

const commands: Command[] = [
	_init,
	_build,
	_install,
	_publish
];

const defaultArgs = {
	'--verbose': Boolean,
	'-v': "--verbose",
	'--help': Boolean,
	'-h': "--help"
};

async function execute(argv: string[]) {
	const command = commands.find(c => c.names.includes(argv[0]));
	if (!command) {
		_log.error(`Couldn't find command ${argv[0]}`);
		printHelp("<command>", defaultArgs);
		return;
	}

	const argTypes = Object.assign(command.args, defaultArgs);
	const args = _parseArgs(argTypes, { argv: argv.slice(1) });

	if (!!args["--help"]) {
		printHelp(argv[0], argTypes);
		return;
	}

	try {
		await command.run(args);
	} catch (e) {
		console.error(e);
		process.exit(1);
	}
}

function printHelp(command: string, args: any) {
	_log.log(`Minecraft Packager v${_pkg.version}`);
	_log.log(`usage: ${_pkg.name} ${command} [...options]\n`);

	for (const [arg, type] of Object.entries(args)) {
		if (typeof type === "string")
			continue;

		const typeName = type != Boolean
			? (type instanceof Array
				? '[...' + type[0].name + ']'
				: '<' + (type as Function).name.toLowerCase() + '>')
			: '';

		_log.log(`  ${arg} ${typeName}`);
	}

	_log.log();
}

const argv = process.argv.slice(2);
if (argv.length > 0 && !['-h', '--help'].includes(argv[0]))
	execute(argv);
else printHelp("<command>", defaultArgs);
