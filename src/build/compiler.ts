import * as _fsutil from '../utils/fs';
import { promises as _fs } from "../utils/fs";
import _path from 'path';
import _os from "os";

import _fm from "front-matter";
import _njk from "nunjucks";
import YAML from 'yaml';

import * as _log from "../utils/logger";
import { compileCollections, compileFrontmatter, compileNunjucks, middleware, MiddlewareFn, minifyJSON, minifyMcfunction, lintMcfunction } from './middleware';
import { CompilerOptions, getCompilerOptions, getOutDir, PackageType, SourceFile, EXT_BINARY } from "./base";

async function lsRecursive(path: string) : Promise<string[]> {
	const ret: string[] = [];
	const dir = await _fs.readdir(_path.resolve(path)).catch(() => null);
	if (!dir) return [];

	for await (const entry of dir) {
		const entryPath = _path.join(path, entry.toString());
		const entryStat = await _fs.stat(_path.resolve(entryPath));
		if (entryStat.isDirectory()) {
			// is a directory: recurse downwards
			const files = await lsRecursive(entryPath);
			ret.push(...files);
		} else if (entryStat.isFile()) {
			// is a file: add to array
			ret.push(entryPath);
		}
	}

	return ret;
}

async function compileData(opts: CompilerOptions) : Promise<any> {
	// build data tree...
	const dataDir = _path.join(opts.cwd, opts.dataDir);
	const data = {
		pkg: opts
	};

	for (const file of await lsRecursive(dataDir)) {
		// mkdir? (except it's a javascript object)
		const obj = _path.dirname(_path.relative(dataDir, file))
			.split('/')
			.filter(key => key != '.')
			.reduce<any>((curr, key) => {
				return key in curr ? curr[key] : (curr[key] = {});
			}, data);

		const key = _path.basename(file).replace(/\..*$/, '');
		if (file.endsWith(".yml")) {
			obj[key] = YAML.parse(await _fs.readFile(_path.resolve(file), { encoding: 'utf8' }) as string);
		} else {
			_log.warning(`Data file type not supported: ${file}`);
		}
	}

	_log.verbose("data =", JSON.stringify(data, null, 4));

	return data;
}

export async function writeFile(file: SourceFile) {
	await _fsutil.mkdirs(_path.dirname(file.name));

	const out = _path.resolve(file.name);
	if (file.text) {
		await _fs.writeFile(out, Buffer.from(file.text, 'utf8'));
	} else if (file.buffer) {
		await _fs.writeFile(out, file.buffer);
	} else {
		_log.error(file.name, "No data provided to write to this file!")
	}
}

async function readFile(name: string, opts: CompilerOptions, data: any) : Promise<SourceFile> {
	const outFile = _path.join(getOutDir(opts), _path.relative(opts.srcDir, name));
	_log.verbose(`${_path.relative(opts.cwd, name)} -> ${_path.relative(opts.cwd, outFile)}`);
	//await _fsutil.mkdirs(_path.resolve(_path.dirname(outFile)));

	const file: SourceFile = {
		name: outFile,
		opts,
		data,
	};

	// use buffer for binary files
	if (EXT_BINARY.some(ext => file.name.endsWith(ext))) {
		file.buffer = await _fs.readFile(_path.resolve(name));
	} else {
		file.text = await _fs.readFile(_path.resolve(name), { encoding: 'utf8' }) as string;
	}

	return file;
}

async function compileSource(opts: CompilerOptions, data: any) : Promise<string[]> {
	// compile source files...
	const outDir = _path.join(opts.cwd, opts.outDir, opts.type, ({
		resourcepack: "assets",
		datapack: "data"
	})[opts.type]);

	// clean any previous files
	try {
		if ((await _fs.stat(_path.resolve(outDir)))?.isDirectory())
			await _fsutil.rimraf(outDir);
	} catch (e) {
		// dir does not exist
	}

	const outFiles: string[] = [];
	const output: MiddlewareFn = async (file) => {
		outFiles.push(file.name);
		await writeFile(file);
	};

	const callback = middleware([compileFrontmatter, compileCollections, compileNunjucks, minifyMcfunction, minifyJSON, lintMcfunction, output]);

	const files = await lsRecursive(opts.srcDir);
	await Promise.all(files.map(file => readFile(file, opts, data).then(callback)));

	return outFiles;
}

export async function compile(cwd: string, type: PackageType) : Promise<string[]> {
	const opts = await getCompilerOptions(cwd, type);
	if (opts.zipFile == "null") {
		// soft-fail: zip for type not defined
		return [];
	}

	const data = await compileData(opts);
	const srcFiles = await compileSource(opts, data);
	return srcFiles;
}
