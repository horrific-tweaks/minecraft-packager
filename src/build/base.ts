import { promises as _fs } from "../utils/fs";
import _path from "path";

import * as _mcversion from '../utils/mcversion';
import { getName } from "../registry";

// binary file extensions not to be rendered with nunjucks
export const EXT_BINARY = [".nbt", ".ogg", ".png"];

export const TYPES = ["resourcepack", "datapack"] as const;
export type PackageType = typeof TYPES[number];

export type CompilerOptions = {
	cwd: string,
	type: PackageType,
	srcDir: string,
	dataDir: string,
	outDir: string,

	id: string,
	name: string,
	version: string,
	format: string,
	description: string,
	author: string,
	dependencies: string[],
	zipFiles: { [type in PackageType]?: string },
	zipFile: string,

	minify: {
		mcfunction: boolean,
		json: boolean,
	}
}

export type FrontMatterOptions = {
	collection?: string,
	filename?: string,
};

export type SourceFile = {
	name: string,
	opts: CompilerOptions,
	data?: any,

	text?: string,
	buffer?: Buffer|Uint8Array,
};

export async function getCompilerOptions(cwd: string, type: PackageType) : Promise<CompilerOptions> {
	const pkgFile = _path.resolve(cwd, 'package.json');
	const pkgText = await _fs.readFile(pkgFile, { encoding: 'utf8' }) as string;
	const pkg = JSON.parse(pkgText);

	const opts: CompilerOptions = Object.assign({
		cwd, type,
		srcDir: _path.join(cwd, type),
		dataDir: '_data',
		outDir: '_out',

		id: _path.basename(cwd),
		name: pkg.name || _path.basename(cwd),
		version: pkg.version || "0.0.0",
		description: pkg.description || "",
		author: pkg.author || "",
		format: _mcversion.resolveVersion(type, _mcversion.latestFormat[type]),
		dependencies: [],
		zipFiles: {},

		minify: {},
	}, pkg.config || {});

	opts.zipFile = opts.zipFiles[type] || getName(opts);
	return opts as CompilerOptions;
}

export function getOutDir(opts: CompilerOptions) {
	return _path.join(opts.cwd, opts.outDir, opts.type, ({
		resourcepack: "assets",
		datapack: "data"
	})[opts.type]);
}
