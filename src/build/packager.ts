import * as _fsutil from '../utils/fs';
import { promises as _fs } from "../utils/fs";
import _path from "path";
import _os from "os";

import JSZip from "jszip";
import _merge from "deepmerge";

import _log from "../utils/logger";
import { CompilerOptions, getCompilerOptions, PackageType } from "./base";
import { resolveFormat } from "../utils/mcversion";
import { resolveLocal } from "../registry";

async function resolveConflict(opts: CompilerOptions, file: string, prevText: string) {
	const path = _path.relative(_path.join(opts.cwd, opts.outDir, opts.type), file);
	const newText = await _fs.readFile(file, { encoding: 'utf8' }) as string;

	if (_path.extname(file) == ".json") {
		const prevJson = JSON.parse(prevText);
		const newJson = JSON.parse(newText);

		// deep-merge lang/tag data
		if (["lang", "tags"].includes(path.split('/')[2])) {
			_log.warning(`Merging JSON file ${path}`);

			const outJson = _merge(prevJson, newJson);
			const outJsonStr = JSON.stringify(outJson, null, 4);
			await _fs.writeFile(file, Buffer.from(outJsonStr, 'utf8'));
			return;
		}

		// merge any model overrides
		if (["models"].includes(path.split('/')[2])) {
			_log.warning(`Merging JSON model file ${path}`);
			prevJson.overrides = _merge<any[]>(prevJson.overrides || [], newJson.overrides || [])
				.sort((a, b) => (a?.predicate?.custom_model_data||0) - (b?.predicate?.custom_model_data||0));

			const outJsonStr = JSON.stringify(prevJson, null, 4);
			await _fs.writeFile(file, Buffer.from(outJsonStr, 'utf8'));
			return;
		}
	}

	_log.warning(`Warning: overwritten file ${path} is specified by another resource pack.`);
}

async function pkgDependencies(opts: CompilerOptions, files: string[]) : Promise<string[]> {
	const outDir = _path.join(opts.cwd, opts.outDir, opts.type);

	for (const pkgName of opts.dependencies) {
		_log.verbose(`+ << ${pkgName}`);

		const zipFile = await resolveLocal(opts.cwd, opts.type, pkgName);
		const zipBuffer = await _fs.readFile(_path.resolve(zipFile));
		const zip = await JSZip.loadAsync(zipBuffer);

		const entries: JSZip.JSZipObject[] = [];
		zip.forEach((_, file) => {
			if (file.dir || _path.basename(file.name) == "pack.mcmeta")
				return;

			entries.push(file);
		});

		// extract all files from zip
		const conflicts: {[key: string]: string} = {};
		for (const entry of entries) {
			const entryPath = _path.join(outDir, _path.join('.', entry.name));

			try { // check for file conflicts
				const entryText = await _fs.readFile(_path.resolve(entryPath), { encoding: 'utf8' }) as string;
				conflicts[entryPath] = entryText;
			} catch (e) {
				// file doesn't exist; no conflict
			}

			// extract (& overwrite) file to outDir
			const buffer = await entry.async("uint8array");
			await _fsutil.mkdirs(_path.dirname(entryPath));
			await _fs.writeFile(_path.resolve(entryPath), buffer);

			if (!files.includes(entryPath))
				files.push(entryPath);
		}

		// resolve conflicting entries
		await Promise.all(Object.entries(conflicts).map(([key, text]) => resolveConflict(opts, key, text)));
	}

	return files;
}

async function pkgSource(opts: CompilerOptions, files: string[]) : Promise<Uint8Array> {
	const zip = new JSZip();
	const zipDir = _path.join(opts.cwd, opts.outDir);
	const outDir = _path.join(opts.cwd, opts.outDir, opts.type);

	// remove previous packages
	const outDirEntries = await _fs.readdir(_path.resolve(zipDir)).catch(() => null);
	if (outDirEntries) for await (const file of outDirEntries) {
		const fileName = file.toString();
		if (fileName.endsWith(`.${opts.type}.zip`))
			await _fs.unlink(_path.resolve(zipDir, fileName));
	}

	// add all source files
	for (const file of files) {
		const fileKey = _path.relative(outDir, file);
		const buffer = await _fs.readFile(_path.resolve(file));

		zip.file(fileKey, buffer);
		_log.verbose(`+ ${fileKey}`);
	}

	// add package metadata
	zip.file("pack.mcmeta", Buffer.from(JSON.stringify({
		pack: {
			pack_format: resolveFormat(opts.type, opts.format),
			description: opts.description
		}
	}), 'utf8'));

	if (opts.type == "resourcepack") {
		// add pack.png thumbnail
		const pngPath = _path.resolve(opts.cwd, "pack.png");
		try {
			const buffer = await _fs.readFile(pngPath) as Buffer;
			zip.file("pack.png", buffer);
		} catch (e) {
			// file does not exist
		}
	}

	// generate zip
	const zipData = await zip.generateAsync({ type: 'uint8array' });
	await _fs.writeFile(_path.resolve(zipDir, opts.zipFile), zipData);
	return zipData;
}

export async function pkg(cwd: string, type: PackageType, files: string[]) : Promise<Uint8Array> {
	const opts = await getCompilerOptions(cwd, type);
	if (opts.zipFile == "null" || (files.length == 0 && opts.dependencies.length == 0)) {
		// soft-fail: no zip file to package
		return new Uint8Array();
	}

	const pkgFiles = await pkgDependencies(opts, files);
	return await pkgSource(opts, pkgFiles);
}
