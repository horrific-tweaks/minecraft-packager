import { expect } from 'chai';
import * as _filters from './filters';

describe('filters.ts', () => {
	describe("dump_nbt()", () => {
		it("removes quotes from properties", () => {
			const nbt = _filters.dump_nbt({ "hi": 1234 });

			expect(nbt).to.equal("{hi:1234}");
		});

		it("should unescape integer array syntax", () => {
			const nbt = _filters.dump_nbt(["I;-1234", 1234, 1234]);

			expect(nbt).to.equal("[I;-1234,1234,1234]");
		});

		it("should not unescape other array entries", () => {
			const test = ["I;-something", 1234, "I;1234"];
			const nbt = _filters.dump_nbt(test);

			expect(nbt).to.equal(`["I;-something",1234,"I;1234"]`);
		});
	});
});
