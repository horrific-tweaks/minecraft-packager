// << data | dump_nbt >> dumps the JSON info in minecraft's nbt syntax
export function dump_nbt(data: any) {
	return JSON.stringify(data)
		// strip quotes from property names
		.replace(/(?<![\\:])(\"(?:[^\r\n:^"]|\\")+\")\s*:/gm, (match) => match.slice(1, -2) + ':')
		// fix integer array syntax: ["I; 0", 0] -> [I; 0, 0]
		.replace(/\[\"(I; *-?\d+)\"/g, "[$1")
		// remove line breaks
		.replace(/\r?\n|\r/g, '');
}
