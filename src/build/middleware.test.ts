import { expect } from 'chai';
import { CompilerOptions, SourceFile } from './base';
import { minifyJSON, minifyMcfunction } from './middleware';

const opts = {
	minify: {
		mcfunction: true,
		json: true,
	}
} as CompilerOptions;

describe("middleware.ts", () => {
	describe("minifyJSON()", () => {
		it("should minify JSON structures", async () => {
			const test = {
				name: 'test.json',
				text: '  \n     {         "test":     "hello world"\n }\n\n',
				opts
			} as SourceFile;

			const expected = '{"test":"hello world"}';

			await minifyJSON(test, async ({text}) => {
				expect(text).to.equal(expected);
			});
		});
	});

	describe("minifyMcfunction()", () => {
		it("should remove comments from .mcfunctions", async () => {
			const test = {
				name: 'test.mcfunction',
				text: '# hello, I am a comment\nhello world',
				opts
			} as SourceFile;

			const expected = 'hello world';

			await minifyMcfunction(test, async ({text}) => {
				expect(text).to.equal(expected);
			});
		});

		it("should remove empty lines from .mcfunctions", async () => {
			const test = {
				name: 'test.mcfunction',
				text: '\nhello\n    \t\n world\n\n',
				opts
			} as SourceFile;

			const expected = 'hello\n world\n';

			await minifyMcfunction(test, async ({text}) => {
				expect(text).to.equal(expected);
			});
		});
	});
});
