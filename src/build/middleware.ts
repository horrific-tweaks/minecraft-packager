import _path from "path";

import _fm from "front-matter";
import _njk from "nunjucks";

import { SourceFile } from './base';
import * as _filters from './nunjucks/filters';
import _log from "../utils/logger";

const MAX_COMMAND_LENGTH = 255;

export type CallbackFn = (file: SourceFile) => Promise<void>;
export type MiddlewareFn = (file: SourceFile, next: CallbackFn) => Promise<void>;
export function middleware(callbacks: MiddlewareFn[]) {
	let wrappedCallback: CallbackFn = async () => {};

	for (const callback of callbacks.reverse()) {
		const prevCallback = wrappedCallback;
		wrappedCallback = async (file: SourceFile) => {
			await callback(file, prevCallback)
				.catch((e) => _log.error(`Failed to compile '${file.name}'`, e));
		};
	}

	return wrappedCallback;
}

/**
 * Creates a middleware function that is only applied to the specified file extension
 */
function withExt(extension: string, callback: MiddlewareFn) : MiddlewareFn {
	return (file: SourceFile, next: CallbackFn) => {
		if (_path.extname(file.name) == extension) {
			return callback(file, next);
		} else {
			return next(file);
		}
	};
}

export const compileFrontmatter: MiddlewareFn = async ({text, data, ...file}, next) => {
	if (text) {
		const { attributes, body } = _fm(text);

		data = Object.assign({}, data, attributes);
		text = body;
	}

	await next({...file, text, data});
};

export const compileCollections: MiddlewareFn = async ({text, data, ...file}, next) => {
	if (text && _path.basename(file.name).startsWith("_")) {
		let collection = data;
		(data.collection || "").split(".").forEach((key: string) => {
			if (key.length > 0 && collection)
				collection = collection[key];
		});

		await Promise.all(Object.entries(collection).map(async ([key, item], index) => {
			// combine iteration variables with global data
			const itemData = Object.assign({}, data, { key, item, index });
			const itemFile = { ...file, text, data: itemData };

			// generate new filename
			const format = data.filename || `<< key >>${_path.extname(file.name)}`;
			const formatName = renderNunjucks({...itemFile, text: format});
			itemFile.name = _path.dirname(file.name) + _path.sep + formatName;

			await next(itemFile);
		}));
	} else {
		await next({...file, text, data});
	}
};

function renderNunjucks(file: SourceFile) : string {
	if (!file.text) throw "renderNunjucks: File text not provided";

	const env = new _njk.Environment(
		new _njk.FileSystemLoader(_path.join(file.opts.srcDir, file.opts.type, "_templates")),
		{
			autoescape: false,
			tags: {
				blockStart: '<%',
				blockEnd: '%>',
				variableStart: '<<',
				variableEnd: '>>',
				commentStart: '<#',
				commentEnd: '#>'
			}
		}
	);

	// add defined nunjucks filters/utils
	for (const [name, func] of Object.entries(_filters)) {
		env.addFilter(name, func);
	}

	return env.renderString(file.text, file.data);
}

export const compileNunjucks: MiddlewareFn = async (file, next) => {
	if (file.text) {
		file.text = renderNunjucks(file);
	}

	await next(file);
}

export const minifyMcfunction = withExt(".mcfunction", ({opts, text, ...file}, next) => {
	if (text && opts.minify.mcfunction != false) {
		const EMPTY_LINE = /^\s*\n/gm;
		const COMMENT = /^\s*#.*\n/gm;

		text = text.replace(EMPTY_LINE, '').replace(COMMENT, '');
	}

	return next({...file, opts, text});
});

export const minifyJSON = withExt(".json", ({opts, text, ...file}, next) => {
	if (text && opts.minify.json != false) {
		// minify the JSON text
		text = JSON.stringify(JSON.parse(text));
	}

	return next({...file, opts, text});
});

export const lintMcfunction = withExt(".mcfunction", ({opts, text, ...file}, next) => {
	if (text) {
		for (const line in text.split('\n')) {
			if (line.length > MAX_COMMAND_LENGTH) {
				const remaining = line.split('').splice(MAX_COMMAND_LENGTH).join('');
				throw `Command length exceeds ${MAX_COMMAND_LENGTH} characters: '${remaining}' will be cut off`;
			}
		}
	}

	return next({...file, opts, text});
});
