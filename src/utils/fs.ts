import { isBrowser } from 'browser-or-node';
import _path from 'path';

import _diskfs from 'fs';
import LightningFS from '@isomorphic-git/lightning-fs';

export const fs = isBrowser ? new LightningFS('mc-packager') : _diskfs;
export const promises = fs.promises;
export default fs;

export async function rimraf(dir: string) : Promise<void> {
	const stat = await promises.stat(dir);
	if (stat.isFile() || stat.isSymbolicLink()) {
		await promises.unlink(dir);
		return;
	}

	const files = await promises.readdir(dir);
	for (const file of files) {
		const filePath = _path.join(dir, file.toString());
		await rimraf(filePath);
	};
}

export async function mkdirs(dir: string) : Promise<void> {
	const parts = _path.resolve(dir).split(_path.sep);

	let path = '';
	for (const part of parts) {
		if (part.length == 0)
			continue;

		path += _path.sep + part;
		try { // check if dir exists
			await promises.stat(path);
		} catch (e) { // create dir
			await promises.mkdir(path)
				.catch(e => null); // dir might already exist
		}
	}
}
