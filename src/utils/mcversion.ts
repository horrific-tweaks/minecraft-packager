import { PackageType } from "../build/base";

const _versions = {
	"resourcepack": {
		'1.[6-8].*': 1,
		"1.(9|10).*": 2,
		"1.(11|12).*": 3,
		"1.(13|14).*": 4,
		"1.(15.*|16.[0-1])": 5,
		"1.16.[2-5]": 6,
		"1.17.*": 7,
		"1.18.*": 8,
		"1.19.*": 9,
	},
	"datapack": {
		'1.[6-8].*': 1,
		"1.(9|10).*": 2,
		"1.(11|12).*": 3,
		"1.(13|14).*": 4,
		"1.(15.*|16.[0-1])": 5,
		"1.16.[2-5]": 6,
		"1.17.*": 7,
		"1.18.[0-1]": 8,
		"1.18.2": 9,
		"1.19.*": 10,
	}
};

export const latestFormat = {
	"resourcepack": Object.keys(_versions["resourcepack"]).length,
	"datapack": Object.keys(_versions["datapack"]).length,
};

export function resolveFormat(type: PackageType, version: string|number) : number {
	if (version.toString().split(".").length == 2)
		version += ".0"; // "1.17" -> "1.17.0"

	for (const [matchStr, format] of Object.entries(_versions[type])) {
		const regexStr = matchStr.trim()
			.replace('.', '\\.')
			.replace('*', '\\d+');

		if (new RegExp(regexStr).test(version.toString()))
			return format;
	}

	// if none found, return latest version
	return latestFormat[type];
}

export function resolveVersion(type: PackageType, format: number) : string {
	return Object.keys(_versions[type])[format-1]
		.replace(/\[(\d)\-\d\]/, '$1')
		.replace(/\(([\w\.\*]+)\|[\w\.\*]+\)/, '$1')
		.replace(/\.\*$/, '')
		.replace('*', '0');
}
