import { expect } from 'chai';
import { resolveFormat, resolveVersion, latestFormat } from './mcversion';
import { TYPES } from "../build/base";

describe("mcversion.ts", () => {
	for (const type of TYPES) {
		describe(type, () => {
			it(`should resolve full version numbers`, () => {
				expect(resolveFormat(type, "1.14.4")).to.equal(4);
			});

			it("should resolve partial version numbers", () => {
				expect(resolveFormat(type, "1.16")).to.equal(5);
			});

			it("resolves format numbers", () => {
				expect(resolveVersion(type, 8).startsWith("1.18")).to.be.true;
			});

			describe("Supported formats:", () => {
				for (let format = 1; format <= latestFormat[type]; format++) {
					let version = resolveVersion(type, format);

					it(`converts ${version} to format ${format}`, () => {
						expect(resolveFormat(type, version)).to.equal(format);
					});
				}
			});
		});
	}
});
