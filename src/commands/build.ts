import { TYPES } from "../build/base";
import { compile } from "../build/compiler";
import { pkg } from "../build/packager";

async function run(args: any) {
	const types = TYPES.filter(type => (args["--type"] || type) == type);
	const srcDir = process.cwd();

	for (const type of types) {
		const files = await compile(srcDir, type);
		await pkg(srcDir, type, files);
	}
}

export default {
	names: ["build"],
	help: ``,
	args: {
		'--type': String
	},
	run
}
