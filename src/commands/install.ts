import { promises as _fs } from "fs";
import _path from "path";
import _os from "os";

import _pkg from "../../package.json";
import { TYPES, getCompilerOptions } from "../build/base";
import _build from './build';

import _log from '../utils/logger';

const mcDirDefault = {
	"Linux": "~/.minecraft",
	"Darwin": "~/Library/Application Support/minecraft",
	"Windows_NT": "%APPDATA%\\.minecraft"
}[_os.type()]

async function run(args: any) {
	const types = TYPES.filter(type => (args["--type"] || type) == type);

	const mcDirArg: string = args["--mcdir"] || process.env.MINECRAFT_HOME || mcDirDefault;
	const mcDir = _path.resolve(mcDirArg.split(_path.sep)
		.map(p => p == '~' ? _os.homedir() : p)
		.map(p => p == '%APPDATA%' ? process.env.APPDATA : p)
		.join(_path.sep));

	const mcWorld = _path.join(mcDir, "saves", args["--world"] || "default");

	const targetDirs = {
		resourcepack: _path.join(mcDir, "resourcepacks"),
		datapack: _path.join(mcWorld, "datapacks")
	};

	const srcDir = process.cwd();

	// build package & install...
	await _build.run(args);

	for (const type of types) {
		const opts = await getCompilerOptions(srcDir, type);
		if (opts.zipFile == "null") continue; // soft-fail: zip for type not defined

		const zipFile = _path.join(srcDir, opts.outDir, opts.zipFile);
		const targetFile = _path.join(targetDirs[type], opts.zipFile);

		await _fs.copyFile(zipFile, targetFile);

		_log.info(`Installed: ${opts.zipFile} -> ${_path.relative(_os.homedir(), targetFile)}`);
	}
}

export default {
	names: ["install"],
	help: ``,
	args: {
		'--type': String,
		'--world': String,
		'--mcdir': String
	},
	run
}