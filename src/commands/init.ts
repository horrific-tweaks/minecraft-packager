import { promises as _fs } from "fs";
import _path from "path";

import _pkg from "../../package.json";
import { TYPES } from "../build/base";
import * as _mcversion from '../utils/mcversion';
import { prompt } from '../utils/prompt';

async function run(args: any) {
	const types = TYPES.filter(type => (args["--type"] || type) == type);
	const cwd = process.cwd();
	const dirname = _path.basename(cwd);

	await _fs.writeFile(".nvmrc", "v12.17.0\n");
	await _fs.writeFile(".gitignore", [".DS_Store", ".DS_Store?", "._*", "*~", "*#", ".Spotlight-V100", ".Trashes", "ehthumbs.db", "Thumbs.db", ".vscode/", ".idea/", "pnpm-lock.yaml", "package-lock.json", "package-install.lock", "node_modules/", "_out/", "*.zip", "*.svg", "*.bbmodel"].join("\n") + '\n');
	await _fs.writeFile("package.json", JSON.stringify({
		name: await prompt("package name", dirname),
		version: await prompt("version", "0.0.1"),
		description: await prompt("description"),
		scripts: {
			build: "minecraft-packager build",
			dev: "minecraft-packager install"
		},
		config: {
			format: await prompt("format", _mcversion.resolveVersion("datapack", _mcversion.latestFormat["datapack"])),
			zipFiles: {
				resourcepack: args["--type"] == "datapack" ? "null" : undefined,
				datapack: args["--type"] == "resourcepack" ? "null" : undefined
			}
		},
		author: await prompt("author"),
		license: await prompt("license", "UNLICENSED"),
		devDependencies: {
			"minecraft-packager": '^' + _pkg.version
		}
	}, null, 2) + '\n');
	await _fs.writeFile("Makefile", `
.PHONY: build dev install clean
.DEFAULT_GOAL := dev

ifeq ($(shell test -f "$(HOME)/.nvm/nvm.sh"; echo $$?),0)
NVM := source $(HOME)/.nvm/nvm.sh && nvm use &> /dev/null
else
NVM := true
endif

ifeq (,$(shell $(NVM) && which pnpm))
NPM := $(NVM) && npm
else
NPM := $(NVM) && pnpm
endif

build: install
	$(NPM) run build

dev: install
	$(NPM) run dev

install: package-install.lock

package-install.lock: package.json
	$(NPM) install
	touch package-install.lock

clean:
	rm -rf _out/`.trim() + '\n');

	for (const type of types) {
		await _fs.mkdir(type).catch(() => {});
	}
}

export default {
	names: ["init", "setup"],
	help: ``,
	args: {
		'--type': String
	},
	run
}
